# 第一天：Train ResNet-50 with CIFAR-10 dataset in MindSpore

## Results
***Best accuracy*** (top-1) is at epoch 71:
`0.99995998`

![](_Day1结果截图.png)

## Configs
```python
    "class_num": 10,
    "batch_size": 64,
    "loss_scale": 1024,
    "momentum": 0.9,
    "weight_decay": 1e-4,
    "epoch_size": 90,
    "buffer_size": 100,
    "image_height": 224,
    "image_width": 224,
    "save_checkpoint": True,
    "save_checkpoint_steps": 195,
    "keep_checkpoint_max": 10,
    "lr_init": 0.01,
    "lr_end": 0.00001,
    "lr_max": 0.1,
    "warmup_epochs": 5,
    "lr_decay_mode": "poly"
```

# 第二天：搭建k8s跑通容器和的MindSpore训练任务

![](_Day2结果截图.png)